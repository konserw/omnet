#include <omnetpp.h>
#include <stdlib.h> 
#include <stdio.h> 

class TaskService : public cSimpleModule
{
  private:
    double w;
    cLongHistogram hist;

  protected:
    cMessage *serviceMsg;
    cQueue queueT;

    cOutVector lengthq;

  public:
    TaskService();
    virtual ~TaskService();

  protected:
    virtual void initialize();
    virtual void finish();
    virtual void handleMessage(cMessage *msg);

    void obsluga(cMessage *msg);
    int tau;
    bool isBusy;
};

Define_Module( TaskService );

TaskService::TaskService()
{
    serviceMsg = NULL;
}

TaskService::~TaskService()
{
    cancelAndDelete(serviceMsg);
}

void TaskService::initialize()
{
    queueT.setName("task-queue");
    hist.setName("histogram of returns");
    hist.setRange(0, par("upperHist"));
    hist.setNumCells(par("numCellsHist"));

    tau = par("serviceTime");
    isBusy = false;
}

void TaskService::obsluga(cMessage *msg)
{
    isBusy = true;
    int czasObslugi = msg->getKind() < tau ? msg->getKind() : tau;
    serviceMsg = msg;
    scheduleAt(simTime() + czasObslugi, serviceMsg);
}

void TaskService::handleMessage(cMessage *msg)
{
	// reagowac na: nadejscie zadania, koniec obslugi
    if (msg->isSelfMessage() == false)//(strcmp(msg->getName(), "task") == 0) // przybycie zadania
    {
        if(isBusy)
        {
            queueT.insert(msg);
        }
        else
        {
            obsluga(msg);
        }
	}
	else if(msg->isScheduled() == false)//selfmessage
	{
	    isBusy = false;
	    int czas = msg->getKind() - tau;

        if(czas <= 0) //koniec obslugi
        {
            hist.collect(msg->getSchedulingPriority());
            msg->setKind(0);
            send(msg, "out");
        }
        else
        {
            msg->setKind(czas);
            msg->setSchedulingPriority(msg->getSchedulingPriority()+1);
            queueT.insert(msg);
        }

        if(queueT.isEmpty() == false)
        {
            obsluga((cMessage*)queueT.pop());
            isBusy = true;
        }
	}
    lengthq.record(queueT.getLength());
}

void TaskService::finish()
{
	hist.recordAs("number of returns");

	ev << "Mean number of returns: " << hist.getMean() << endl;
}


