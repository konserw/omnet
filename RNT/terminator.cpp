#include <omnetpp.h>

class Terminator : public cSimpleModule
{
  private:
    long numObs;

  protected:
    virtual void initialize();
    virtual void handleMessage(cMessage *msg);
    virtual void finish();
};

Define_Module( Terminator );

void Terminator::initialize()
{
    numObs=0;
}

void Terminator::handleMessage(cMessage *msg)
{
    numObs+=1;
    delete msg;
 }

void Terminator::finish()
{
    ev << "Total number of tasks: " << numObs << endl;
    ev << "Total time of experiment:   " << simTime() << endl;
}

