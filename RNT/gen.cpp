#include <omnetpp.h>

class TaskSource : public cSimpleModule
{
  private:
    cMessage *sendMessageEvent;

  public:
     TaskSource();
     virtual ~TaskSource();

  protected:
    virtual void initialize();
    virtual void handleMessage(cMessage *msg);
};

Define_Module(TaskSource);

TaskSource::TaskSource()
{
	sendMessageEvent = NULL;
}

TaskSource::~TaskSource()
{
	cancelAndDelete(sendMessageEvent);
}

void TaskSource::initialize()
{
	sendMessageEvent = new cMessage("nextTask");
	scheduleAt(0.0, sendMessageEvent);
	srand((unsigned)time(NULL));
}

void TaskSource::handleMessage(cMessage *msg)
{
	ASSERT(msg==sendMessageEvent);
	int czas=(rand()%74)+1;
	cMessage *m = new cMessage("task");
	m->setKind(czas);
	m->setSchedulingPriority(0);
	send(m, "out");

	scheduleAt(simTime()+(double)par("sendIaTime"), sendMessageEvent);
}


