#include <omnetpp.h>

class UpperFloor : public cSimpleModule
{
  private:
    long nbPass, maxPass;
    cLongHistogram histU; // histogram liczby osob przewozonych w dol
    double w; // zmienna pomocnicza do obliczenia czasu oczekiwania
    cDoubleHistogram histwU; // histogram czasow oczekiwania
    cOutVector lengthqU; // przebieg liczby oczekujacych na winde na gornym pietrze

  protected:
    cMessage *elMsg;
    bool button;
    bool sentButton;
    double czas;

  public:
    UpperFloor();
    virtual ~UpperFloor();

  protected:
    virtual void initialize();
    virtual void finish();
    virtual void handleMessage(cMessage *msg);
};

Define_Module( UpperFloor );

UpperFloor::UpperFloor()
{
	elMsg = nullptr;
	button = false;
	sentButton = false;
	czas = -1;
}

UpperFloor::~UpperFloor()
{
}

void UpperFloor::initialize()
{
    button = false;

	maxPass = par("maxPassengers");
    nbPass = 0;
	elMsg = new cMessage("el");
	send(elMsg, "gate$o");
	histU.setName("histogram passengers downwards");
	histwU.setName("wait time histogram");
	histU.setRange(0, maxPass+1);
}

void UpperFloor::handleMessage(cMessage *msg)
{
    if (strcmp(msg->getName(), "el")==0) // przybycie windy
	{
		for(int i=0; i<msg->getKind(); ++i)
		{
		    cMessage *pass = new cMessage("passanger");
		    scheduleAt(simTime()+(double)par("serviceTime"), pass);
		}
		elMsg = msg;
		elMsg->setKind(0);
		sentButton = false;
	}
	else if(strcmp(msg->getName(), "passanger")==0)// przybycie pasazera
	{
	    if(czas < 0) czas = simTime().dbl();
		nbPass++;
		delete msg;
		lengthqU.record(nbPass);
	}
	else //button
	{
	    button = true;
	    delete msg;
	}

    if(elMsg != nullptr && elMsg->isScheduled() == false)
    {
        int sent = 0;
        if(nbPass > 0) //ladujemy pasazerow i wciskamy przycisk
        {
            sent = nbPass >  maxPass ? maxPass : nbPass;
            elMsg->setKind(sent);
            nbPass -= sent;
            button = true;

            double waitTime;
            waitTime = simTime().dbl() - czas;
            czas = -1;
            histwU.collect(waitTime);
            lengthqU.record(nbPass);
        }

        if(button == true) //wysylamy winde
        {
            sendDelayed(elMsg, (double)par("rideTime"), "gate$o");
            elMsg = nullptr;
            button = false;
            histU.collect(sent);
        }
    }

    if(elMsg == nullptr && nbPass > 0 && sentButton == false)
    {
        cMessage *m = new cMessage("button");
        send(m, "gate$o");
        sentButton = true;
    }

}

void UpperFloor::finish()
{
	histU.recordAs("Number of passengers downwards");
    histwU.recordAs("Upper floor waiting time");

    ev << "Mean waiting time: " << histwU.getMean() << endl;
    ev << "Mean number of passengers downwards: " << histU.getMean() << endl;
}
