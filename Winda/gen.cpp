#include <omnetpp.h>

class PassengerSource : public cSimpleModule
{
  private:
    cMessage *sendMessageEvent;

  public:
    PassengerSource();
     virtual ~PassengerSource();

  protected:
    virtual void initialize();
    virtual void handleMessage(cMessage *msg);
};

Define_Module(PassengerSource);

PassengerSource::PassengerSource()
{
	sendMessageEvent = NULL;
}

PassengerSource::~PassengerSource()
{
	cancelAndDelete(sendMessageEvent);
}

void PassengerSource::initialize()
{
	sendMessageEvent = new cMessage("sendMessageEvent");
	scheduleAt(0.0, sendMessageEvent);
}

void PassengerSource::handleMessage(cMessage *msg)
{
	ASSERT(msg==sendMessageEvent);

	cMessage *m = new cMessage("passenger");
	send(m, "out");

	scheduleAt(simTime()+(double)par("sendIaTime"), sendMessageEvent);
}


