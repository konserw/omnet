#include <omnetpp.h>

class BottomFloor : public cSimpleModule
{
  private:
    long nbPass, maxPass;
    cLongHistogram histB; // histogram liczby osob przewozonych w gore
    double w; // zmienna pomocnicza do obliczenia czasu oczekiwania
    cDoubleHistogram histwB; // histogram czasow oczekiwania
    cOutVector lengthqB; // przebieg liczby oczekujacych na winde na dolnym pietrze

    bool button;
    bool sentButton;
    cMessage* winda;
    double czas;

  public:
    BottomFloor();
    virtual ~BottomFloor();

  protected:
    virtual void initialize();
    virtual void handleMessage(cMessage *msg);
    virtual void finish();
};

Define_Module( BottomFloor );

BottomFloor::BottomFloor()
{
}

BottomFloor::~BottomFloor()
{
}

void BottomFloor::initialize()
{
    czas = -1;
    button = false;
    sentButton = false;
    winda = nullptr;

	nbPass = 0;
	maxPass = par("maxPassengers");
    histB.setName("histogram passengers upwards");
    histwB.setName("wait time histogram");
    histB.setRange(0, maxPass+1);
}

void BottomFloor::handleMessage(cMessage *msg)
{
	if (msg->arrivedOn("inP")) // przybycie pasazera
	{
	    if(czas < 0) czas = simTime().dbl();
		++nbPass;
		delete msg;
		lengthqB.record(nbPass);
	}
	else if(strcmp(msg->getName(), "el")==0) //przybycie windy
	{
		winda = msg;
		winda->setKind(0);
		sentButton = false;
	}
	else //przycisk
	{
	    button = true;
	    delete msg;
	}

	if(winda != nullptr && winda->isScheduled() == false)
    {
	    int sent = 0;
        if(nbPass > 0) //ladujemy pasazerow
        {
            sent = nbPass >  maxPass ? maxPass : nbPass;
            winda->setKind(sent);
            nbPass -= sent;
            button = true;

            double waitTime;
            waitTime = simTime().dbl() - czas;
            czas = -1;
            histwB.collect(waitTime);
            lengthqB.record(nbPass);
        }

        if(button == true) //wysylamy winde
        {
            sendDelayed(winda, (double)par("rideTime"), "gate$o");
            winda = nullptr;
            button = false;
            histB.collect(sent);
        }
    }

	if(winda == nullptr && nbPass > 0 && sentButton == false)
    {
        cMessage *m = new cMessage("button");
        send(m, "gate$o");
        sentButton = true;
    }
}

void BottomFloor::finish()
{
    histB.recordAs("Number of passengers upwards");
    histwB.recordAs("Bottom floor waiting time");

    ev << "Mean waiting time: " << histwB.getMean() << endl;
    ev << "Mean number of passengers upwards: " << histB.getMean() << endl;

}
